package com.jonny.employe.utils;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.jonny.employe.controllers.PersonController;
import com.jonny.employe.services.PersonService;

public class RestExceptionHandlerTest {

	@Mock
	private PersonService personService;

	@InjectMocks
	PersonController personController;

	private MockMvc mvc;

	@BeforeEach
	void initTest() {
		MockitoAnnotations.openMocks(this);
		mvc = MockMvcBuilders.standaloneSetup(personController).setControllerAdvice(new RestExceptionHandler()).build();
	}

	@Test
	public void noSuchElementException() throws Exception {
		Mockito.when(personController.listPerson()).thenThrow(new NoSuchElementException("Unexpected Exception"));
		mvc.perform(get("/api/v1/person/list")).andExpect(status().is(400));
	}

	@Test
	public void duplicateKeyException() throws Exception {
		Mockito.when(personController.listPerson()).thenThrow(new DuplicateKeyException("Unexpected Exception"));
		mvc.perform(get("/api/v1/person/list")).andExpect(status().is(400));
	}

	@Test
	public void illegalArgumentException() throws Exception {
		Mockito.when(personController.listPerson()).thenThrow(new IllegalArgumentException("Unexpected Exception"));
		mvc.perform(get("/api/v1/person/list")).andExpect(status().is(400));
	}

	@Test
	public void invalidDataException() throws Exception {
		BindingResult bindingResult = Mockito.mock(BindingResult.class);
		Mockito.when(personController.listPerson()).thenThrow(new InvalidDataException(bindingResult));
		mvc.perform(get("/api/v1/person/list")).andExpect(status().is(400));
	}

	@Test
	public void methodArgumentTypeMismatchException() throws Exception {
		MethodArgumentTypeMismatchException ex = mock(MethodArgumentTypeMismatchException.class);
		Mockito.when(personController.listPerson()).thenThrow(ex);
		mvc.perform(get("/api/v1/person/list")).andExpect(status().is(400));
	}

	@Test
	public void exception() throws Exception {
		ArrayIndexOutOfBoundsException ex = mock(ArrayIndexOutOfBoundsException.class);
		Mockito.when(personController.listPerson()).thenThrow(ex);
		mvc.perform(get("/api/v1/person/list")).andExpect(status().is(500));
	}

}
