package com.jonny.employe.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UtilBase64ImplTest {
	
	@Autowired
	UtilBase64 utilBase64;
	
	
	@Test
	void decodeString() {
		String stringBase64 = "am9ubnkgcm9qYXMgZGVsIHJpbw==";
		String decodeString = utilBase64.decodeString(stringBase64);
		Assertions.assertEquals(decodeString, "jonny rojas del rio");
	}

}
