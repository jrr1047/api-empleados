package com.jonny.employe.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.jonny.employe.models.InitView;
import com.jonny.employe.models.SelectOption;
import com.jonny.employe.models.SelectOptionWithData;
import com.jonny.employe.models.SelectOptionWithDataDTO;
import com.jonny.employe.repositories.CountryRepository;
import com.jonny.employe.repositories.DepartamentRepository;
import com.jonny.employe.repositories.TypeIdRepository;


public class DataServiceImplTest {
	
	
	@Mock
	TypeIdRepository typeIdRepository;
	
	@Mock
	DepartamentRepository departamentRepository;
	
	@Mock
	CountryRepository countryRepository;
	
	@InjectMocks
	DataServiceImpl dataServiceImpl = new DataServiceImpl();
	
	@BeforeEach
	void initTest() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void initViewRegister() {
		List<SelectOptionWithData> contriesMock = new ArrayList<>();
		contriesMock.add(new SelectOptionWithDataDTO("1", "COLOMBIA", "co"));
		contriesMock.add(new SelectOptionWithDataDTO("2", "ESTADOS UNIDOS", "eu"));
		
		List<SelectOption> typesIdMock = new ArrayList<>();
		typesIdMock.add(new SelectOptionWithDataDTO("1", "CÉDULA DE CIUDADANÍA"));
		
		List<SelectOption> departamentsMock = new ArrayList<>();
		departamentsMock.add(new SelectOptionWithDataDTO("1", "COMPRAS"));
		departamentsMock.add(new SelectOptionWithDataDTO("1", "INFRAESTRUCTURA"));

		
		Mockito.when(countryRepository.findAllCountries()).thenReturn(contriesMock);
		Mockito.when(typeIdRepository.findAllTypesId()).thenReturn(typesIdMock);
		Mockito.when(departamentRepository.findAllDepartaments()).thenReturn(departamentsMock);
		
		InitView initView = dataServiceImpl.initViewRegister();
		Assertions.assertEquals(initView.getCountries().size(), 2);
		Assertions.assertEquals(initView.getTypesId().size(), 1);
		Assertions.assertEquals(initView.getDepartaments().size(), 2);
	}

}
