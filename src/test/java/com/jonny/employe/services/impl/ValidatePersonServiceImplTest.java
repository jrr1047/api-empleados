package com.jonny.employe.services.impl;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.jonny.employe.models.CheckPerson;
import com.jonny.employe.models.CheckPersonDTO;
import com.jonny.employe.repositories.PersonRepository;
import com.jonny.employe.utils.UtilBase64;

public class ValidatePersonServiceImplTest {
	
	@Mock
	PersonRepository personRepository;
	
	@Mock
	UtilBase64 utilBase64;
	
	@InjectMocks
	ValidatePersonServiceImpl validatePersonServiceImpl;
	
	@BeforeEach
	void initTest() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void validateEmail() {
		String stringEncode = "ampfMTA0N0Bob3RtaWFsLmNvbQ==";
		Mockito.when(utilBase64.decodeString(stringEncode)).thenReturn("jj_1047@hotmial.com");
		Mockito.when(personRepository.checkEmailPerson("jj_1047@hotmial.com")).thenReturn(null);
		CheckPerson result = validatePersonServiceImpl.validateEmail(stringEncode);
		Assertions.assertEquals(result, null);
	}
	
	@Test
	public void validatePerson() {
		String identification = "1047389512";
		Integer typeId = 1;
		Mockito.when(personRepository.findPerson(identification, typeId)).thenReturn(new CheckPersonDTO(1, identification, typeId));
		CheckPerson result = validatePersonServiceImpl.validatePerson(identification, typeId);
		Assertions.assertEquals(result.getId(), 1);
	}

}
