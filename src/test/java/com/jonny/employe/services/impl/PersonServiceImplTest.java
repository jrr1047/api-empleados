package com.jonny.employe.services.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DuplicateKeyException;

import com.jonny.employe.models.CheckPersonDTO;
import com.jonny.employe.models.Person;
import com.jonny.employe.models.PersonDTO;
import com.jonny.employe.repositories.PersonRepository;

public class PersonServiceImplTest {

	@Mock
	ModelMapper modelMapper;

	@Mock
	PersonRepository personRepository;

	@InjectMocks
	PersonServiceImpl personServiceImpl;

	@BeforeEach
	void initTest() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void savePeron() {
		String identification = "1047389512";
		Integer typeId = 1;
		Person person = getPersonMock();
		CheckPersonDTO check = new CheckPersonDTO(1, identification, typeId);
		Mockito.when(personRepository.checkEmailPerson("jj_1047@hotmail.com")).thenReturn(check);
		Mockito.when(personRepository.findPerson(identification, typeId)).thenReturn(check);
		Mockito.when(personRepository.save(person)).thenReturn(person);

		Person result = personServiceImpl.savePeron(person);
		Assertions.assertEquals(result.getId(), 1);

		Person person2 = new Person().setIdentification("1127345765").setTypeId(1).setEmail("person@person.com")
				.setAdmission(setDate(2021, 3, 1));

		Mockito.when(personRepository.existsPersonByEmail("person@person.com")).thenReturn(false);
		Mockito.when(personRepository.save(person2)).thenReturn(person2);

		Person result2 = personServiceImpl.savePeron(person2);
		Assertions.assertEquals(result2.getIdentification(), person2.getIdentification());

		Person person3 = new Person().setIdentification("1127345765").setTypeId(1).setEmail("person3@person.com")
				.setAdmission(setDate(2021, 1, 1));

		Mockito.when(personRepository.existsPersonByEmail("person3@person.com")).thenReturn(true);

		try {
			personServiceImpl.savePeron(person3);
		} catch (Exception e) {
			Assertions.assertEquals(e.getClass(), DuplicateKeyException.class);
		}

	}
	
	@Test
	public void savePeron2(){
		Person person4 = new Person().setIdentification("1127345765").setTypeId(1).setEmail("person4@person.com")
				.setAdmission(setDate(2021, 1, 1));
		
		Mockito.when(personRepository.existsPersonByEmail("person4@person.com")).thenReturn(false);

		try {
			personServiceImpl.savePeron(person4);
		} catch (Exception e) {
			Assertions.assertEquals(e.getClass(), IllegalArgumentException.class);
		}
	}
	
	@Test
	public void savePeron3(){
		Person person5 = new Person().setIdentification("1127345765").setTypeId(1).setEmail("person5@person.com")
				.setAdmission(setDate(2021, 1, 1)).setId(5);
		
		CheckPersonDTO check = new CheckPersonDTO(1, person5.getIdentification(), person5.getTypeId());
		Mockito.when(personRepository.findPerson(person5.getIdentification(), person5.getTypeId())).thenReturn(check);

		try {
			personServiceImpl.savePeron(person5);
		} catch (Exception e) {
			Assertions.assertEquals(e.getClass(), DuplicateKeyException.class);
		}
	}
	
	@Test
	public void savePeron4(){
		Person person5 = new Person().setIdentification("1127345765").setTypeId(1).setEmail("person5@person.com")
				.setAdmission(setDate(2021, 1, 1)).setId(5);
		

		CheckPersonDTO check = new CheckPersonDTO(5, person5.getIdentification(), person5.getTypeId());
		CheckPersonDTO checkEmail = new CheckPersonDTO(2, person5.getIdentification(), person5.getTypeId());
		Mockito.when(personRepository.findPerson(person5.getIdentification(), person5.getTypeId())).thenReturn(check);
		Mockito.when(personRepository.checkEmailPerson("person5@person.com")).thenReturn(checkEmail);

		try {
			personServiceImpl.savePeron(person5);
		} catch (Exception e) {
			Assertions.assertEquals(e.getClass(), DuplicateKeyException.class);
		}
	}

	@Test
	public void findAllPerson() {
		Person person = getPersonMock();

		List<Person> personsMock = new ArrayList<>();
		personsMock.add(person);

		Mockito.when(personRepository.findAll()).thenReturn(personsMock);
		List<PersonDTO> list = personServiceImpl.findAllPerson();
		Assertions.assertEquals(list.size(), 1);
	}

	@Test
	public void deletePerson() {
		personServiceImpl.deletePerson(1);
		verify(personRepository, times(1)).deleteById(1);

	}

	private Date setDate(int year, int month, int day) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);
		return cal.getTime();
	}

	private Person getPersonMock() {
		return new Person().setId(1).setFirstName("JONNY").setOtherName(null).setMiddleName("ROJAS")
				.setLastName("DEL RIO").setIdentification("1047389512").setTypeId(1).setEmail("jj_1047@hotmail.com")
				.setCountry(1).setDepartment(1).setStatus(1).setAdmission(setDate(2021, 3, 1))
				.setCreation(setDate(2021, 3, 10));
	}
}
