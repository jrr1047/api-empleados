package com.jonny.employe.controllers;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.jonny.employe.models.InitView;
import com.jonny.employe.models.SelectOption;
import com.jonny.employe.models.SelectOptionWithData;
import com.jonny.employe.models.SelectOptionWithDataDTO;
import com.jonny.employe.services.DataService;

public class DataControllerTest {
	
	@Mock
	DataService dataService;
	
	@InjectMocks
	DataController dataController = new DataController();
	
	@BeforeEach
	void initTest() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void initViewRegister() {
		
		List<SelectOptionWithData> contriesMock = new ArrayList<>();
		contriesMock.add(new SelectOptionWithDataDTO("1", "COLOMBIA", "co"));
		contriesMock.add(new SelectOptionWithDataDTO("2", "ESTADOS UNIDOS", "eu"));
		
		List<SelectOption> typesIdMock = new ArrayList<>();
		typesIdMock.add(new SelectOptionWithDataDTO("1", "CÉDULA DE CIUDADANÍA"));
		
		List<SelectOption> departamentsMock = new ArrayList<>();
		departamentsMock.add(new SelectOptionWithDataDTO("1", "COMPRAS"));
		departamentsMock.add(new SelectOptionWithDataDTO("1", "INFRAESTRUCTURA"));
		
		InitView initView = new InitView(typesIdMock, contriesMock, departamentsMock);
		
		Mockito.when(dataService.initViewRegister()).thenReturn(initView);
		
		InitView result = dataController.initViewRegister();
		Assertions.assertEquals(result.getCountries().size(), 2);
		Assertions.assertEquals(result.getTypesId().size(), 1);
		Assertions.assertEquals(result.getDepartaments().size(), 2);
	}

}
