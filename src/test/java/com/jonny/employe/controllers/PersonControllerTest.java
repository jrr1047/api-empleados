package com.jonny.employe.controllers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.BindingResult;


import com.jonny.employe.models.Person;
import com.jonny.employe.models.PersonDTO;
import com.jonny.employe.services.PersonService;
import com.jonny.employe.utils.InvalidDataException;

public class PersonControllerTest {

	@Mock
	private PersonService personService;

	@InjectMocks
	PersonController personController;

	@BeforeEach
	void initTest() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void savePerson() {
		Person person = new Person().setId(1).setFirstName("JONNY").setOtherName(null).setMiddleName("ROJAS")
				.setLastName("DEL RIO").setIdentification("1047389512").setTypeId(1).setEmail("jj_1047@hotmail.com")
				.setCountry(1).setDepartment(1).setStatus(1).setAdmission(setDate(2021, 3, 1))
				.setCreation(setDate(2021, 3, 10));

		BindingResult bindingResult = Mockito.mock(BindingResult.class);
		Mockito.when(bindingResult.hasErrors()).thenReturn(false);
		Mockito.when(personService.savePeron(person)).thenReturn(person);

		Person result = personController.savePerson(person, bindingResult);
		Assertions.assertEquals(result.getId(), 1);
	}

	@Test
	public void savePersonWithError() {
		Person person = getPersonMock();

		BindingResult bindingResult = Mockito.mock(BindingResult.class);
		Mockito.when(bindingResult.hasErrors()).thenReturn(true);

		try {
			personController.savePerson(person, bindingResult);
		} catch (Exception e) {
			Assertions.assertEquals(e.getClass(), InvalidDataException.class);
		}

	}
	
	@Test
	public void listPerson() {
		List<PersonDTO> personsMock = new ArrayList<>();
		personsMock.add(new PersonDTO());
		personsMock.add(new PersonDTO());
		Mockito.when(personService.findAllPerson()).thenReturn(personsMock);
		List<PersonDTO> list = personController.listPerson();
		Assertions.assertEquals(list.size(), 2);
	}
	
	@Test
	public void validatePerson() {
		Integer id = 1;
		personController.delete(id);
		verify(personService, times(1)).deletePerson(id);
	}

	private Date setDate(int year, int month, int day) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);
		return cal.getTime();
	}
	
	private Person getPersonMock() {
		return new Person().setId(1).setFirstName("JONNY").setOtherName(null).setMiddleName("ROJAS")
				.setLastName("DEL RIO").setIdentification("1047389512").setTypeId(1).setEmail("jj_1047@hotmail.com")
				.setCountry(1).setDepartment(1).setStatus(1).setAdmission(setDate(2021, 3, 1))
				.setCreation(setDate(2021, 3, 10));
	}

}
