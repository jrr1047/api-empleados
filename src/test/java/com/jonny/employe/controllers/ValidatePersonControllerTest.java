package com.jonny.employe.controllers;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import com.jonny.employe.models.CheckPerson;
import com.jonny.employe.models.CheckPersonDTO;
import com.jonny.employe.services.ValidatePersonService;

public class ValidatePersonControllerTest {

	@Mock
	ValidatePersonService validateEmailService;
	
	@InjectMocks
	ValidatePersonController validatePersonController;

	@BeforeEach
	void initTest() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void validateEmail() {
		String email = "jj_1047@hotmial.com";
		Mockito.when(validateEmailService.validateEmail(email)).thenReturn(null);
		ResponseEntity<CheckPerson>  response = validatePersonController.validateEmail(email);
		Assertions.assertEquals(response.getBody(), null);
	}

	@Test
	public void validatePerson() {
		String identification = "1047389512";
		Integer typeId = 1;
		Mockito.when(validateEmailService.validatePerson(identification, typeId)).thenReturn(new CheckPersonDTO(1, identification, typeId));
		ResponseEntity<CheckPerson> response = validatePersonController.validatePerson(identification, typeId);

		Assertions.assertEquals(response.getBody().getId(), 1);
	}

}
