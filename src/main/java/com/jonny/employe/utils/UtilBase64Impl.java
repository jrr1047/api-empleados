package com.jonny.employe.utils;

import java.util.Base64;

import org.springframework.stereotype.Component;

@Component
public class UtilBase64Impl implements UtilBase64 {

    public String decodeString(String encodedString) {
        byte[] bytes = Base64.getDecoder().decode(encodedString);
        return new String(bytes);
    }
}
