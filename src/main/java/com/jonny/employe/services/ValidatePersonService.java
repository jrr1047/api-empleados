package com.jonny.employe.services;

import com.jonny.employe.models.CheckPerson;

public interface ValidatePersonService {
	CheckPerson validateEmail(String email);
	CheckPerson validatePerson(String identificacion, int typeId);
}
