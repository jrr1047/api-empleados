package com.jonny.employe.services;

import java.util.List;

import com.jonny.employe.models.Person;
import com.jonny.employe.models.PersonDTO;

public interface PersonService {
	
	Person savePeron(Person person);
	
	List<PersonDTO> findAllPerson();
	
	void deletePerson(Integer id);
}
