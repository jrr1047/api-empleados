package com.jonny.employe.services.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.jonny.employe.models.Person;
import com.jonny.employe.models.PersonDTO;
import com.jonny.employe.models.CheckPerson;
import com.jonny.employe.repositories.PersonRepository;
import com.jonny.employe.services.PersonService;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public Person savePeron(Person person) {
		CheckPerson valid = this.personRepository.findPerson(person.getIdentification(), person.getTypeId());
		if (valid != null) {
			if (valid.getId() != person.getId()) {
				throw new DuplicateKeyException("Ya existe empleado con indetificaciòn:" + person.getIdentification());
			}
		}

		boolean validEmail = this.validEmailPerson(person.getId(), person.getEmail());
		if (validEmail) {
			throw new DuplicateKeyException("Ya existe empleado con email:" + person.getEmail());
		}

		boolean validDate = validateDateAdmission(person.getAdmission());
		if (validDate) {
			throw new IllegalArgumentException("Solo se permite inglesos de hace 30 dias.");
		}

		return this.personRepository.save(person);
	}

	@Override
	public List<PersonDTO> findAllPerson() {
		List<Person> persons = (List<Person>) this.personRepository.findAll();
		return persons.stream().map(x -> modelMapper.map(x, PersonDTO.class)).collect(Collectors.toList());
	}

	@Override
	public void deletePerson(Integer id) {
		this.personRepository.deleteById(id);
	}

	private boolean validateDateAdmission(Date date) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -30);
		Date oldDate = c.getTime();
		return date.before(oldDate);
	}

	private boolean validEmailPerson(Integer id, String email) {
		if (id == null) {
			return this.personRepository.existsPersonByEmail(email);
		} else {
			CheckPerson check = this.personRepository.checkEmailPerson(email);
			return id != check.getId();
		}
	}

}
