package com.jonny.employe.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jonny.employe.models.CheckPerson;
import com.jonny.employe.repositories.PersonRepository;
import com.jonny.employe.services.ValidatePersonService;
import com.jonny.employe.utils.UtilBase64;

@Service
public class ValidatePersonServiceImpl implements ValidatePersonService {

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private UtilBase64 utilBase64;

	@Override
	public CheckPerson validateEmail(String data) {
		String email = utilBase64.decodeString(data);
		return this.personRepository.checkEmailPerson(email);
	}

	@Override
	public CheckPerson validatePerson(String identificacion, int typeId) {
		return this.personRepository.findPerson(identificacion, typeId);
	}

}
