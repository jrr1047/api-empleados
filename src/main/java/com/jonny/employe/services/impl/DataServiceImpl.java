package com.jonny.employe.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jonny.employe.models.InitView;
import com.jonny.employe.models.SelectOption;
import com.jonny.employe.models.SelectOptionWithData;
import com.jonny.employe.repositories.CountryRepository;
import com.jonny.employe.repositories.DepartamentRepository;
import com.jonny.employe.repositories.TypeIdRepository;
import com.jonny.employe.services.DataService;

@Service
public class DataServiceImpl implements DataService {
	
	@Autowired
	private TypeIdRepository typeIdRepository;
	
	@Autowired
	private DepartamentRepository departamentRepository;
	
	@Autowired
	private CountryRepository countryRepository;

	@Override
	public InitView initViewRegister() {
		List<SelectOptionWithData> countries = this.countryRepository.findAllCountries();
		List<SelectOption> types = this.typeIdRepository.findAllTypesId();
		List<SelectOption> departamets = this.departamentRepository.findAllDepartaments();
		return new InitView(types, countries, departamets);
	}

}
