package com.jonny.employe.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "persons")
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	@NotEmpty(message = "Se requiere campo nombre.")
	@Size(max = 20, message = "Logitud maxima de nombre es de 20 caracteres.")
	@Pattern(regexp = "^[A-Z_ ]+$", message = "Campo nombre solo permite mayuscaulas, sin Ñ ni acentos.")
	@Column(length = 20, nullable = false, name = "first_name")
	private String firstName;

	@Size(max = 50, message = "Logitud maxima de otros nombres es de 50 caracteres.")
	@Pattern(regexp = "^[A-Z\\s]*$", message = "Campo otros nombres  no se permiten Ññ ni acentos.")
	@Column(length = 50, nullable = true, name = "other_name")
	private String otherName;

	@NotEmpty(message = "Se requiere campo primer apellido.")
	@Size(max = 20, message = "Logitud maxima de primer apellido es de 20 caracteres.")
	@Pattern(regexp = "^[A-Z_ ]+$", message = "Campo primer apellido solo permite mayuscaulas, sin Ñ ni acentos.")
	@Column(length = 20, nullable = false, name = "middle_name")
	private String middleName;

	@NotEmpty(message = "Se requiere campo segundo apellido.")
	@Size(max = 20, message = "Logitud maxima de segundo apellido es de 20 caracteres.")
	@Pattern(regexp = "^[A-Z_ ]+$", message = "Campo segundo apellido solo permite mayuscaulas, sin Ñ ni acentos.")
	@Column(length = 20, nullable = false, name = "last_name")
	private String lastName;

	@NotNull(message = "El campo tipo de identificaciòn no puede ser nulo.")
	@Positive(message = "El campo tipo de identificaciòn debe ser un numero positivo.")
	@Range(min = 1, message = "El campo tipo de identificaciòn su valor minimo debe ser 1.")
	@Column(nullable = false, name = "type_id")
	private Integer typeId;

	@NotEmpty(message = "Se requiere campo identification.")
	@Size(max = 20, message = "Logitud maxima de la identificaciòn es de 20 caracteres.")
	@Pattern(regexp = "^[a-zA-Z\\-0-9]+$", message = "Campo identificaciòn no permite Ññ ni acentos.")
	@Column(length = 20, nullable = false)
	private String identification;

	@NotEmpty(message = "Se requiere campo correo electronico.")
	@Email(message = "Formato de correo electronico no valido.")
	@Size(max = 300, message = "Logitud maxima del campo correo electronico es de 300 caracteres.")
	@Column(length = 300, nullable = false, unique = true)
	private String email;

	@NotNull(message = "El campo pais no puede ser nulo.")
	@Positive(message = "El campo pais debe ser un numero positivo.")
	@Range(min = 1, message = "El campo pais su valor minimo debe ser 1.")
	@Column(nullable = false, name = "country_id")
	private Integer country;

	@NotNull(message = "El campo area no puede ser nulo.")
	@Positive(message = "El campo area debe ser un numero positivo.")
	@Range(min = 1, message = "El campo area su valor minimo debe ser 1.")
	@Column(nullable = false, name = "department_id")
	private Integer department;

	@Column(name = "status")
	private Integer status;

	@NotNull(message = "El campo fecha de ingreso no puede ser nulo.")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date admission;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date creation;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date upgrade;

	public Integer getId() {
		return id;
	}

	public Person setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public Person setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getOtherName() {
		return otherName;
	}

	public Person setOtherName(String otherName) {
		this.otherName = otherName;
		return this;
	}

	public String getMiddleName() {
		return middleName;
	}

	public Person setMiddleName(String middleName) {
		this.middleName = middleName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public Person setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public Person setTypeId(Integer typeId) {
		this.typeId = typeId;
		return this;
	}

	public String getIdentification() {
		return identification;
	}

	public Person setIdentification(String identification) {
		this.identification = identification;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public Person setEmail(String email) {
		this.email = email;
		return this;
	}

	public Integer getCountry() {
		return country;
	}

	public Person setCountry(Integer country) {
		this.country = country;
		return this;
	}

	public Integer getDepartment() {
		return department;
	}

	public Person setDepartment(Integer department) {
		this.department = department;
		return this;
	}

	public Integer getStatus() {
		return status;
	}

	public Person setStatus(Integer status) {
		this.status = status;
		return this;
	}

	public Date getAdmission() {
		return admission;
	}

	public Person setAdmission(Date admission) {
		this.admission = admission;
		return this;
	}

	public Date getCreation() {
		return creation;
	}

	public Person setCreation(Date creation) {
		this.creation = creation;
		return this;
	}

	public Date getUpgrade() {
		return upgrade;
	}

	public Person setUpgrade(Date upgrade) {
		this.upgrade = upgrade;
		return this;
	}

}
