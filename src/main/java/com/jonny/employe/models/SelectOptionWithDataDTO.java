package com.jonny.employe.models;

public class SelectOptionWithDataDTO implements SelectOptionWithData {

	private String id;
	private String name;
	private String data;

	public SelectOptionWithDataDTO() {
	}

	public SelectOptionWithDataDTO(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public SelectOptionWithDataDTO(String id, String name, String data) {
		this.id = id;
		this.name = name;
		this.data = data;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getData() {
		return this.data;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setData(String data) {
		this.data = data;
	}

}
