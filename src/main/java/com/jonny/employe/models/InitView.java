package com.jonny.employe.models;

import java.util.List;

public class InitView {
	
	private List<SelectOption> typesId;
	private List<SelectOptionWithData> countries;
	private List<SelectOption> departaments;
	
	public InitView(List<SelectOption> typesId, List<SelectOptionWithData> countries, List<SelectOption> departaments) {
		super();
		this.typesId = typesId;
		this.countries = countries;
		this.departaments = departaments;
	}

	public List<SelectOption> getTypesId() {
		return typesId;
	}

	public void setTypesIid(List<SelectOption> typesId) {
		this.typesId = typesId;
	}

	public List<SelectOptionWithData> getCountries() {
		return countries;
	}

	public void setCountries(List<SelectOptionWithData> countries) {
		this.countries = countries;
	}

	public List<SelectOption> getDepartaments() {
		return departaments;
	}

	public void setDepartaments(List<SelectOption> departaments) {
		this.departaments = departaments;
	}

}
