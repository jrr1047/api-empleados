package com.jonny.employe.models;

public interface CheckPerson {
	Integer getId();

	String getIdentification();

	Integer getTypeId();
}
