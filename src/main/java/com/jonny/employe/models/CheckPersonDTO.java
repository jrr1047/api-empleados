package com.jonny.employe.models;

public class CheckPersonDTO implements CheckPerson {

	Integer id;
	String identification;
	Integer typeId;

	public CheckPersonDTO() {
	}

	public CheckPersonDTO(Integer id, String identification, Integer typeId) {
		this.id = id;
		this.identification = identification;
		this.typeId = typeId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

}
