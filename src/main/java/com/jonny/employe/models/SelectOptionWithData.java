package com.jonny.employe.models;

public interface SelectOptionWithData extends SelectOption{
	
	String getData();
	
}
