package com.jonny.employe.models;

public interface SelectOption {
	String getId();

	String getName();
}
