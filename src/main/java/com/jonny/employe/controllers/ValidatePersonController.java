package com.jonny.employe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jonny.employe.models.CheckPerson;
import com.jonny.employe.services.ValidatePersonService;

@RestController
@RequestMapping("api/v1/validate_person")
public class ValidatePersonController {
	
	@Autowired
	private ValidatePersonService validateEmailService;
	
	@GetMapping("/email/{email}")
	public ResponseEntity<CheckPerson>  validateEmail(@PathVariable String email) {
		return ResponseEntity.ok(this.validateEmailService.validateEmail(email));
	}
	
	@GetMapping("/person/{identificacion}/{typeId}")
	public ResponseEntity<CheckPerson> validatePerson(@PathVariable String identificacion, @PathVariable int typeId) {
		return ResponseEntity.ok(this.validateEmailService.validatePerson(identificacion, typeId));
	}

}
