package com.jonny.employe.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jonny.employe.models.InitView;
import com.jonny.employe.services.DataService;

@RestController
@RequestMapping("api/v1/data")
public class DataController {
	
	@Autowired
	private DataService dataService;
	
	@GetMapping("/register")
	public InitView initViewRegister() {
		return this.dataService.initViewRegister();
	}

}
