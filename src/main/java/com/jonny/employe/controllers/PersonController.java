package com.jonny.employe.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jonny.employe.models.Person;
import com.jonny.employe.models.PersonDTO;
import com.jonny.employe.services.PersonService;
import com.jonny.employe.utils.InvalidDataException;

@RestController
@RequestMapping("api/v1/person")
public class PersonController {

	@Autowired
	private PersonService personService;

	@PostMapping("/save")
	public Person savePerson(@Valid @RequestBody Person person, BindingResult result) {
		if(result.hasErrors()){
			throw new InvalidDataException(result);
		}
		return this.personService.savePeron(person);
	}

	@GetMapping("/list")
	public List<PersonDTO> listPerson() {
		return this.personService.findAllPerson();
	}

	@DeleteMapping("/delete")
	public ResponseEntity<?> delete(@RequestParam Integer id) {
		this.personService.deletePerson(id);
		return ResponseEntity.ok().build();
	}

}
