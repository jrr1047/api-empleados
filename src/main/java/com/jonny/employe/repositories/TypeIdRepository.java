package com.jonny.employe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jonny.employe.models.SelectOption;
import com.jonny.employe.models.TypeId;

public interface TypeIdRepository extends CrudRepository<TypeId, Integer>{
public static final String QUERY_TYPEID = "select new com.jonny.employe.models.TypeId(d.id, d.name) from TypeId d where d.status = 1";
	
	@Query(value = QUERY_TYPEID)
	public List<SelectOption> findAllTypesId();
}
