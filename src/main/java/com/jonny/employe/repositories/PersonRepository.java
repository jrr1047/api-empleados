package com.jonny.employe.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.jonny.employe.models.Person;
import com.jonny.employe.models.CheckPerson;

public interface PersonRepository extends CrudRepository<Person, Integer>{ 
	public static final String QUERY_VALID_ID = "select id, identification, type_id as typeid from persons where identification = :identification and type_id = :typeid ";
	public static final String QUERY_VALID_EMAIL = "select id, identification, type_id as typeid from persons where email = :email ";
	
	boolean existsPersonByEmail(String email);
	
	@Query(value = QUERY_VALID_EMAIL, nativeQuery = true)
	CheckPerson checkEmailPerson(@Param("email") String email);
	
	@Query(value = QUERY_VALID_ID, nativeQuery = true)
	CheckPerson findPerson(@Param("identification") String identification, @Param("typeid") int typeid);

}

