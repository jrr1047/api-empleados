package com.jonny.employe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jonny.employe.models.Departament;
import com.jonny.employe.models.SelectOption;

public interface DepartamentRepository extends CrudRepository<Departament, Integer>{ 
public static final String QUERY_DEPARTAMENTS = "select new com.jonny.employe.models.Departament(d.id, d.name) from Departament d where d.status = 1";
	
	@Query(value = QUERY_DEPARTAMENTS)
	public List<SelectOption> findAllDepartaments();
}
