package com.jonny.employe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jonny.employe.models.Country;
import com.jonny.employe.models.SelectOptionWithData;

public interface CountryRepository extends CrudRepository<Country, Integer>{ 
	public static final String QUERY_COUNTRY = "select id, name, domain as data from countries where status = 1";
	
	@Query(value = QUERY_COUNTRY, nativeQuery = true)
	public List<SelectOptionWithData> findAllCountries();
}
